use fs_err as fs;
use miette::IntoDiagnostic;

fn main() -> miette::Result<()> {
    let path = std::env::args().nth(1).unwrap(); // TODO: Clap
    let contents = fs::read_to_string(path).into_diagnostic()?;
    let mut doc: kdl::KdlDocument = contents.parse()?;
    // println!("{doc}");

    debug3::dbg!(doc);

    // doc.fmt();
    // println!("{doc}");

    // doc = kdl::KdlDocument::new();
    // doc.nodes_mut().push(kdl::KdlNode::new("instance"));

    // println!("{doc}");

    Ok(())
}
